const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
require("dotenv").config();
require("./util/Init_MonoDB.js");
const authRoute= require('./Router/user.router')
const scheduleRoute= require('./Router/schedule.router')
const app = express();
app.use(cors());
app.use(morgan("dev"));
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb", extended: true }));
app.use(function (req, res, next) {
  // Website you wish to allow to connected
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});
app.get("/", async (req, res, next) => {
  res.send("welcome to schedule app ....");
});
app.use("/auth", authRoute);
app.use("/schedule", scheduleRoute);
const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Server running.... on port ${PORT}`);
});
