const mongoose = require("mongoose");
const userDB = require("../models/userModel")
const Objects=mongoose.Types.ObjectId
const scheduleModel= require("../models/scheduleModel")
const restrictionsModel = require("../models/restrictionsModel");
module.exports={
    addSchedule:async(req,res)=>{
     try{
         if(req.body.id){
             foundObject=await scheduleModel.findById(req.body.id)
             if(foundObject.length!=0){
                 await scheduleModel.updateOne({_id:foundObject._id},{...req.body})
                 res.json({status: 'update'})
             }else{
                 res.status(500).json({status: 'error',error:"can't update"})
             }
         }else{
            const userId=Objects(req.payload.aud)
            const data=req.body.data
            const obj = JSON.parse(data)
            for(let i=0;i<obj.length;i++){
                isMatch=await scheduleModel.find({userId:userId,day:obj[i].day,
                    startTime:obj[i].startTime,
                    endTime:obj[i].endTime})
                if(isMatch.length==0){
                    const newSchedule=new scheduleModel({
                        userId:userId,
                        day:obj[i].day,
                        type:obj[i].type,
                        startTime:obj[i].startTime,
                        endTime:obj[i].endTime
                    })
                    await newSchedule.save()
                }
            }
            res.json({status:"saved"})
         }
     }catch(err){
         res.status(500).json({status:"error",error:"can't save"})
     }
    },
    listSchedule:async(req,res)=>{
        try{
            const id=Objects(req.payload.aud)
            const nonWorkingSchedule=await scheduleModel.find({type:"1",userId:id});
            const workingSchedule=await scheduleModel.find({type:"0",userId:id});
            res.json({workingSchedule:workingSchedule,nonWorkingSchedule:nonWorkingSchedule})
        }catch(err){
            res.status(500).json({status:"error",error:"can't list non"})
        }
    },
    restirictApp:async(req,res)=>{
        try{
            if(req.body.id){
                foundObject=await restrictionsModel.findById(req.body.id)
                if(foundObject.length!=0){
                    await scheduleModel.updateOne({_id:foundObject._id},{...req.body})
                    res.json({status: 'update'})
                }else{
                    res.status(500).json({status: 'error',error:"can't update"})
                } 
            }else{
                const data=req.body.restirictAppData
                const obj = JSON.parse(data)
                for(let i=0;i<obj.length;i++){
                    isMatch=await restrictionsModel.find({scheduleId:Objects(obj[i].scheduleId)
                        ,appName:obj[i].appName})
                    if(isMatch.length==0){
                        const newRestrictions=new restrictionsModel({
                            scheduleId:Objects(obj[i].scheduleId),
                            appName:obj[i].appName,
                            timePeriod:obj[i].timePeriod
                        })
                        await newRestrictions.save()
                    }
                }
                res.json({status:"saved"})
            }
        }catch(err){
            res.status(500).json({status:"error",error:"error to restrict"})
        }
    },
    listRestriction:async(req,res)=>{
        try{
            const restriction=await scheduleModel.aggregate([
                {
                    $lookup:
                    {
                      from:"restrictions",
                      localField:"_id" ,
                      foreignField:"scheduleId" ,
                      as:"restrict"
                    }
                },
                {
                    $match:{userId:Objects(req.payload.aud)}
                },
                {
                    $match:{type:req.body.type}
                }
            ])
            res.json(restriction)
        }catch(err){
            res.status(500).json({status:"error",error:"error to listRestriction"})
        }
    },
    deleteSchedule:async(req,res)=>{
        try{
            const scheduleId=Objects(req.body.scheduleId);
            await scheduleModel.deleteOne({_id:scheduleId})
            await restrictionsModel.deleteOne({scheduleId:scheduleId})
            res.json({status:"deleted"})
        }catch(err){
            res.json({status:"error",error:"error to deleteSchedule"})
        }
    },
    deleteRestriction:async(req,res)=>{
        try{
            const restrictionId=Objects(req.body.restrictionId);
            await restrictionsModel.deleteOne({_id:restrictionId})
            res.json({status:"deleted"})
        }catch(err){
            res.json({status:"error",error:"error to delete restriction"})
        }
    },
}