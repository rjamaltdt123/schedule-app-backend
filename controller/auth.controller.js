const userDB = require("../models/userModel");
const { signAccessToken } = require("../middleware/auth.middleware");

module.exports = {
    register: async (req, res) => {
      try {
        const { email, password } =
          req.body;
        if (
          !email ||
          !password
        ) {
          res.json({
            status: "error",
            error: "data missing",
          });
        } else {
          await userDB.find({ email: email }, (err, result) => {
            if (err) {
              res.json(err);
            }
            if (result.length === 0) {
              const newuser = new userDB({
                email: email,
                password: password
              });
              newuser.save((err,data)=>{
                  res.json({status: 'success'})
              })
            } else {
              res.status(500).send({
                status: "error",
                error: "email already in use",
              });
            }
          });
        }
      } catch (err) {
        res.status(500).send({
          status: "error",
          error: 500,
        });
      }
    },
    login: async (req, res) => {
      try {
        const { email, password } = req.body;
        const user = await userDB.findOne({ email: email });
        if (!user) {
          return res.status(500).json({
            status: "error",
            error: "user not registered",
          });
        }
        const isMatch = await user.isValidPassword(password);
        if (!isMatch) {
          return res.status(500).json({
            status: "error",
            error: "Username/password not valid",
          });
        } else {
            console.log("_id",user._id)
          const accessToken = await signAccessToken(String(user._id));
          return res.json({
            status: "ok",
            id: user._id,
            email : user.email,
            accessToken: accessToken,
          });
        }
      } catch (err) {
        console.log(err);
        res.status(500).json({
          status: "error",
          error: 500,
        });
      }
    },
}