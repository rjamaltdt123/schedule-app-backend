const mongoose = require("mongoose");
const restrictionSchema = new mongoose.Schema({
    scheduleId: {
      type: Object,
      required: true,
    },
    appName: {
        type: String,
        required: true,
      },
    timePeriod: {
      type: String,
      required: true,
    },
  });
  module.exports = mongoose.model("restriction", restrictionSchema);