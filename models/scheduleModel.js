const mongoose = require("mongoose");
const scheduleSchema = new mongoose.Schema({
    userId: {
      type: Object,
      required: true,
    },
    type: {
        type: String,
        required: true,
      },
    day: {
      type: String,
      required: true,
    },
    startTime: {
        type: String,
        required: true,
      },
    endTime: {
        type: String,
        required: true,
    }
  });
  module.exports = mongoose.model("schedule", scheduleSchema);