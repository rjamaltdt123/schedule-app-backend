const JWT = require("jsonwebtoken");
const createError = require("http-errors");

module.exports = {
    //sign token with user id
    signAccessToken: (userId) => {
      console.log(userId)
      return new Promise((resolve, reject) => {
        const payload = { userId };
        const sercet = process.env.ACCESS_TOKEN_SECRET;
        const options = {
          expiresIn: "1y",
          issuer: "pickurpage.com",
          audience: userId,
        };
        JWT.sign(payload, sercet, options, (err, token) => {
          if (err) {
            console.log(err.message);
            reject(createError.InternalServerError());
            return;
          }
          resolve(token);
        });
      });
    },
    //verify the access token
    verifyAccessToken: (req, res, next) => {
      if (!req.headers["authorization"]) {
        return next(createError.Unauthorized());
      }
      const authHeader = req.headers["authorization"];
      const bearerToken = authHeader.split(" ");
      const token = bearerToken[1];
      JWT.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
        if (err) {
          const message =
            err.name === "JsonWebTokenError" ? "Unauthorized" : err.message;
          console.log(message);
          return next(createError.Unauthorized(message));
        }
        req.payload = payload;
        next();
      });
    },
}