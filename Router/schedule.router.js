const express = require("express");
const scheduleRouter = express.Router();
const scheduleController = require("../controller/schedule.controller");
const {verifyAccessToken}=require("../middleware/auth.middleware")

scheduleRouter.post(
    "/addSchedule",
    verifyAccessToken,
    scheduleController.addSchedule
    );
scheduleRouter.post(
    "/listSchedule",
    verifyAccessToken,
    scheduleController.listSchedule
    );
scheduleRouter.post(
    "/restrictApp",
    verifyAccessToken,
    scheduleController.restirictApp
    );
scheduleRouter.post(
    "/listRestrictApp",
    verifyAccessToken,
    scheduleController.listRestriction
    );
scheduleRouter.post(
    "/deleteSchedule",
    verifyAccessToken,
    scheduleController.deleteSchedule
    );
scheduleRouter.post(
    "/deleteRestriction",
    verifyAccessToken,
    scheduleController.deleteRestriction
    );
module.exports = scheduleRouter;